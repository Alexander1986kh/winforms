﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComboBoxNorthWindLineTester
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            cBoxBisnessEntity_Id.DropDownStyle = ComboBoxStyle.DropDown;//Стиль, необходимый для работы автозаполнения
            cBoxBisnessEntity_Id.AutoCompleteMode = AutoCompleteMode.Suggest;//Помощь автозаполнением
            cBoxBisnessEntity_Id.AutoCompleteSource = AutoCompleteSource.ListItems;
            DataSet ds = new DataSet();//Структура памяти для временного хранения данных базы
            string connectionString = @"Data Source=DESKTOP-C6D4823\SQLEXPRESS;Initial Catalog=AdventureWorks2008R2;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();
                using (SqlCommand command = new SqlCommand("select * from[Person].[Person]", con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);//Инструмент для заполнения DataSet 
                    adapter.Fill(ds, "Person");
                }

            }
            //Создание привязок
            cBoxBisnessEntity_Id.DataSource = ds.Tables["Person"];
            cBoxBisnessEntity_Id.DisplayMember = "BusinessEntityID";
            cBoxBisnessEntity_Id.ValueMember = "BusinessEntityID";

            txtFirstName.DataBindings.Add(new Binding("Text", ds.Tables["Person"], "FirstName"));
        }

        private void cBoxBisnessEntity_Id_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}