﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComboBoxTester
{
    public class Customer
    {
        protected Customer()
        {

        }
        public Customer(string id_men, string nick_name, string phone, string email)
        {
            this.id_men = id_men;
            this.nick_name = nick_name;
            this.phone = phone;
            this.email = email;
        }
        //
        #region Entity part
        //тексты запросов:
        //select All
        public static String selectAllCustomers()
        {
            StringBuilder sqltext = new StringBuilder();
            sqltext.Append(" SELECT  [db28pr5].[dbo].[CUSTOMERS].ID,");
            sqltext.Append(" [db28pr5].[dbo].[CUSTOMERS].EMAIL, ");
            sqltext.Append(" [db28pr5].[dbo].[CUSTOMERS].NICK_NAME, ");
            sqltext.Append(" [db28pr5].[dbo].[CUSTOMERS].PHONE, ");
            sqltext.Append(" [db28pr5].[dbo].[CUSTOMERS].ID_MEN, ");
            sqltext.Append(" [db28pr5].[dbo].[MEN].LNAME, ");
            sqltext.Append(" [db28pr5].[dbo].[MEN].NAME, ");
            sqltext.Append(" [db28pr5].[dbo].[MEN].YEAR ");
            sqltext.Append(" FROM  ");
            sqltext.Append(" [db28pr5].[dbo].[CUSTOMERS] ");
            sqltext.Append(" LEFT JOIN [db28pr5].[dbo].[MEN] on  ");
            sqltext.Append(" [db28pr5].[dbo].[MEN].ID=[db28pr5].[dbo].[CUSTOMERS].ID_MEN ");
            return sqltext.ToString();
        }
        //select One для Id
        public static String selectCustomersForId(int id)
        {
            StringBuilder sqltext = new StringBuilder();
            sqltext.Append(selectAllCustomers());
            sqltext.Append(" where [db28pr5].[dbo].[CUSTOMERS].ID=" + id.ToString());
            return sqltext.ToString();
        }
        //insert
        public static String getInserQueryForCustomer(Customer newCustomer)
        {
            return string.Empty;
        }
        //update
        public static String getUpdateQueryForCustomer(Customer newCustomer)
        {
            return String.Empty;
        }
        public static String getUpdateQueryForCustomer()
        {
            return String.Empty;//возвращает параметризированный запрос (еще не было)
        }
        public string getDeleteTextForCustomer(int id)
        {
            return String.Empty;
        }
        //delete
        #endregion
        //
        public override string ToString()
        {
            return String.Format("свдения о записи с Id={0} ({1},{2}): {3},{4},{5}.", Id_men, Lname, Sname, Nick_name, Phone, Email);
        }
        #region fields
        private string nick_name;

        public string Nick_name
        {
            get { return nick_name; }
            set { nick_name = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string id_men;

        public string Id_men
        {
            get { return id_men; }
            set { id_men = value; }
        }
        private string lname;

        public string Lname
        {
            get { return lname; }
            set { lname = value; }
        }
        private string sname;

        public string Sname
        {
            get { return sname; }
            set { sname = value; }
        }
        private int year;

        public int Year
        {
            get { return year; }
            set { year = value; }
        }
        #endregion
    }
}
