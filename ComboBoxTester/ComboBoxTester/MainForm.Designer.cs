﻿namespace ComboBoxTester
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbox1 = new System.Windows.Forms.ComboBox();
            this.cboxCustomers = new System.Windows.Forms.ComboBox();
            this.cboxCustomerClass = new System.Windows.Forms.ComboBox();
            this.cboxPlayers = new System.Windows.Forms.ComboBox();
            this.txtBoxCustomer = new System.Windows.Forms.TextBox();
            this.txtBoxPlayer = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbox1
            // 
            this.cbox1.FormattingEnabled = true;
            this.cbox1.Location = new System.Drawing.Point(23, 23);
            this.cbox1.Name = "cbox1";
            this.cbox1.Size = new System.Drawing.Size(228, 21);
            this.cbox1.TabIndex = 0;
            // 
            // cboxCustomers
            // 
            this.cboxCustomers.FormattingEnabled = true;
            this.cboxCustomers.Location = new System.Drawing.Point(25, 67);
            this.cboxCustomers.Name = "cboxCustomers";
            this.cboxCustomers.Size = new System.Drawing.Size(226, 21);
            this.cboxCustomers.TabIndex = 1;
            // 
            // cboxCustomerClass
            // 
            this.cboxCustomerClass.FormattingEnabled = true;
            this.cboxCustomerClass.Location = new System.Drawing.Point(27, 109);
            this.cboxCustomerClass.Name = "cboxCustomerClass";
            this.cboxCustomerClass.Size = new System.Drawing.Size(224, 21);
            this.cboxCustomerClass.TabIndex = 2;
            // 
            // cboxPlayers
            // 
            this.cboxPlayers.FormattingEnabled = true;
            this.cboxPlayers.Location = new System.Drawing.Point(29, 152);
            this.cboxPlayers.Name = "cboxPlayers";
            this.cboxPlayers.Size = new System.Drawing.Size(222, 21);
            this.cboxPlayers.TabIndex = 3;
            // 
            // txtBoxCustomer
            // 
            this.txtBoxCustomer.Location = new System.Drawing.Point(271, 110);
            this.txtBoxCustomer.Name = "txtBoxCustomer";
            this.txtBoxCustomer.Size = new System.Drawing.Size(135, 20);
            this.txtBoxCustomer.TabIndex = 4;
            // 
            // txtBoxPlayer
            // 
            this.txtBoxPlayer.Location = new System.Drawing.Point(271, 153);
            this.txtBoxPlayer.Name = "txtBoxPlayer";
            this.txtBoxPlayer.Size = new System.Drawing.Size(135, 20);
            this.txtBoxPlayer.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 199);
            this.Controls.Add(this.txtBoxPlayer);
            this.Controls.Add(this.txtBoxCustomer);
            this.Controls.Add(this.cboxPlayers);
            this.Controls.Add(this.cboxCustomerClass);
            this.Controls.Add(this.cboxCustomers);
            this.Controls.Add(this.cbox1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbox1;
        private System.Windows.Forms.ComboBox cboxCustomers;
        private System.Windows.Forms.ComboBox cboxCustomerClass;
        private System.Windows.Forms.ComboBox cboxPlayers;
        private System.Windows.Forms.TextBox txtBoxCustomer;
        private System.Windows.Forms.TextBox txtBoxPlayer;
    }
}

