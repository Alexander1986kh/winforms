﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;//для работы с файлом App.config - из System.configuration.dll
using System.Data.SqlClient;// из драйвера MS SQL Server-а
using System.Data.SqlTypes;//System.Data.dll

namespace ComboBoxTester
{
    public partial class MainForm : Form
    {
        private BindingList<Customer> list;
        private BindingList<Player> players;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region Внешний вид Выпадающего списка
            cboxCustomers.DropDownStyle = ComboBoxStyle.DropDown;//Стиль, необходимый для работы автозаполнения
            cboxCustomers.AutoCompleteMode = AutoCompleteMode.Suggest;//Помощь автозаполнением
            cboxCustomers.AutoCompleteSource = AutoCompleteSource.ListItems;//Источник автозаполнения
            #endregion
            #region Соединение с базой данных
            DataSet ds = new DataSet();//Структура памяти для временного хранения данных базы
            string connectionString = @"Data Source=DESKTOP-C6D4823\SQLEXPRESS;Initial Catalog=db28pr8;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                StringBuilder str_b = new StringBuilder();
                str_b.Append("select* from[dbo].[CUSTOMERS] as customers ");
                str_b.Append("left join [dbo].[MEN] as men ");
                str_b.Append(" on customers.ID_MAN = men.ID;");

                using (SqlCommand command = new SqlCommand(str_b.ToString(), con))
                {
      /*
                                    using (SqlDataReader reader = command.ExecuteReader())
                                    {
                                        List<Customer> customers = new List<Customer>();
                                        while (reader.Read())
                                        {
                                            int v1 = reader.GetInt32(0);
                                            string nick = reader["NICK_NAME"].ToString();//reader.GetString(1);
                                            string phone = reader["PHONE"].ToString();//reader.GetString(2);
                                            string email = reader["EMAIL"].ToString();//DateTime datevalue = reader.GetDateTime(2);
                                            string id_men = reader["ID_MEN"].ToString();
                                            Customer customer = new Customer(id_men, nick, phone, email);
                                            customer.Sname = reader["LNAME"].ToString();
                                            customer.Lname = reader["NAME"].ToString();
                                            int year;
                                            Int32.TryParse(reader["ID_MEN"].ToString(), out year);
                                            customer.Year = year;
                                            customers.Add(customer);
                                            Console.WriteLine("поля из выборки {0} {1} {2}", v1, nick, phone);
                                            Console.WriteLine("создан customer: {0}", customer);
                                        }
                                    }
                                        
    */
                    SqlDataAdapter adapter = new SqlDataAdapter(command);//Инструмент для заполнения DataSet 
                    adapter.Fill(ds, "CUSTOMERS");//Само заполнение DataSEt из результов запроса
                    //ds-название DataSet, "CUSTOMERS"- название таблички в DataSet
                }
                
            #endregion
                #region Привязки
                //Для второго ComboBox
                cboxCustomers.DataSource = ds.Tables["CUSTOMERS"];//источник данных для отображения - как правило DataTable
                cboxCustomers.DisplayMember = "NICK_NAME";//имя столбца, знач.кот.отобр.
                cboxCustomers.ValueMember = "ID";//имя id столбца для поиска строки, из кот.берем столбец
                //"ID"-имя столбца из запроса, PrimaryKey
                BindingSource bs = new BindingSource(ds, "CUSTOMERS");//Создаем привязку
                cboxCustomers.DataBindings.Add("SelectedValue", bs, "ID");
                cboxCustomers.DataBindings.Add(new Binding("Text", bs, "NICK_NAME"));
                bs.Position = 1;//выделели элемент в ComboBox-е программно-
                //при запуске программы

                //Для превого CmboBox
                cbox1.DataSource = ds.Tables["CUSTOMERS"];//источник данных для отображения - как правило DataTable
                cbox1.DisplayMember = "NAME";//имя столбца, знач.кот.отобр.
                cbox1.ValueMember = "ID";//имя id столбца для поиска строки, из кот.берем столбец
                //Третий ComboBox

                list = new BindingList<Customer>();
                list.Add( new Customer("0","adm","911","admin@adminsworld.com"));
                list.Add(new Customer("1","user1","007","superuser@usersworld.com"));

                cboxCustomerClass.DataSource = list;
                cboxCustomerClass.DisplayMember = "Nick_name";//Здесь это имя public свойства
                txtBoxCustomer.DataBindings.Add(new Binding("Text", list, "Nick_name"));
                //Четвертый ComboBox
                players = new BindingList<Player>();
                players.Add(new Player("Иванов", 10));
                players.Add(new Player("Петров", 20));
                players.Add(new Player("Сидоров", 30));

                cboxPlayers.DataSource = players;
                cboxPlayers.DisplayMember = "PlayerName";


               
                txtBoxPlayer.DataBindings.Add(new Binding("Text", players, "PlayerName"));

                #endregion
            }
        }
    }
}
