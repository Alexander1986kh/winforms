﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComboBoxTester0809
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=PC36-8-P;Initial Catalog=db28pr8;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT TOP 1000 [CLIENT_ID],[CLIENT_NAME] FROM [db28pr8].[CLIENTS]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        //int v1 = reader.GetInt32(0);
                        string v1 = reader.GetString(0);
                        string v2 = reader.GetString(1);
                        DateTime v3 = reader.GetDateTime(2);
                        Console.WriteLine("{0} {1} {2}", v1, v2, v3);
                    }
                }
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmdSelect = new SqlCommand();
                    SqlParameter currencyCodeParam = new SqlParameter("@pCultureID", System.Data.SqlDbType.NVarChar);
                    string cultureIdCode = "ar";
                    currencyCodeParam.Value = cultureIdCode;
                    cmdSelect.Parameters.Add(currencyCodeParam);
                    cmdSelect.CommandText = "SELECT TOP 1000 [CLIENT_ID],[CLIENT_NAME] FROM [db28pr8].[CLIENTS] where CLIENT_ID=@pCultureID";
                    cmdSelect.Connection = conn;
                    conn.Open();
                    SqlDataReader reader = cmdSelect.ExecuteReader();
                    while (reader.Read())
                    {
                        //int v1 = reader.GetInt32(0);
                        string v1 = reader.GetString(0);
                        string v2 = reader.GetString(1);
                        DateTime v3 = reader.GetDateTime(2);
                        Console.WriteLine("{0} {1} {2}", v1, v2, v3);
                    }
                    reader.Close();
                    conn.Close();
                }
            }
        }
    }
}
