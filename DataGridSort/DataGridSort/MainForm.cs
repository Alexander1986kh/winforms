﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridSort
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            dgv.AllowUserToAddRows = false;

            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgv.Dock = DockStyle.Fill;
            dgv.SortCompare += new DataGridViewSortCompareEventHandler(this.dgv_my_SortCompare);
            PopulateDataGridView();

        }

        private void dgv_my_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            // Try to sort based on the cells in the current column.
            //Сортировка по умолчанию, сравниваем строки столбца, где был клик
            e.SortResult = System.String.Compare(
                e.CellValue1.ToString(), e.CellValue2.ToString());
            // If the cells are equal, sort based on the ID column.
            //Далее добавляем свою сортировку- если это не столбец-идентификатор- сортируем по Id
            if (e.SortResult == 0 && e.Column.Name != "ID")
            {
                e.SortResult = System.String.Compare(
                    dgv.Rows[e.RowIndex1].Cells["ID"].Value.ToString(),
                    dgv.Rows[e.RowIndex2].Cells["ID"].Value.ToString());
            }
            e.Handled = true;//Отработано
        }

        public void PopulateDataGridView()
        {
            // Add columns to the DataGridView.
            dgv.ColumnCount = 3;
            // Set the properties of the DataGridView columns.
            dgv.Columns[0].Name = "ID";
            dgv.Columns[1].Name = "Name";
            dgv.Columns[2].Name = "City";
            dgv.Columns["ID"].HeaderText = "ID";
            dgv.Columns["Name"].HeaderText = "Name";
            dgv.Columns["City"].HeaderText = "City";
            // Add rows of data to the DataGridView.
            dgv.Rows.Add(new string[] { "1", "Parker", "Seattle" });
            dgv.Rows.Add(new string[] { "2", "Parker", "New York" });
            dgv.Rows.Add(new string[] { "3", "Watson", "Seattle" });
            dgv.Rows.Add(new string[] { "4", "Jameson", "New Jersey" });
            dgv.Rows.Add(new string[] { "5", "Brock", "New York" });
            dgv.Rows.Add(new string[] { "6", "Conner", "Portland" });
            // Autosize the columns.
            dgv.AutoResizeColumns();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
          
        }
        private void dgv_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (e.Value.ToString() == "M")
            {
                e.Value = "1";
            }
            else if (e.Value.ToString() == "W")
            {
                e.Value = "0";
            }
        }
        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dGrV = (DataGridView)sender;
            string nameColumn = dGrV.Columns[e.ColumnIndex].Name;
            //цвет выбеделния ячейки
            /*
            if (dGrV.Rows[e.RowIndex].Cells[e.ColumnIndex] == dGrV.CurrentCell)
            {
                e.CellStyle.SelectionBackColor = Color.DarkSeaGreen;
            }
            */
            if ((e.Value != null) && (nameColumn == "Sex") && (e.Value.ToString() == "1"))
            {
                e.Value = "M".ToUpper();

                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.ForeColor = Color.WhiteSmoke;
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                e.CellStyle.Font = new Font("Times New Roman", 7, FontStyle.Bold);
            }
            if ((e.Value != null) && (nameColumn == "Sex") && (e.Value.ToString() == "0"))
            {
                e.Value = "W".ToUpper();

                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.ForeColor = Color.WhiteSmoke;
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                e.CellStyle.Font = new Font("Times New Roman", 7, FontStyle.Bold);
            }
        }

    }
}
