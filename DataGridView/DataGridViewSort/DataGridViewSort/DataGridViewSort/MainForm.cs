﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridViewSort
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgv.Dock = DockStyle.Fill;

            dgv.SortCompare +=  new DataGridViewSortCompareEventHandler(this.dgv_SortCompare);

            PopulateDataGridView();
        }
        private void dgv_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            // сортировка по умолчанию - сравниваем соседние строки того столбца, где был клик
            e.SortResult = System.String.Compare(e.CellValue1.ToString(), e.CellValue2.ToString());
            //далее добавляем свою сортировку: если это не столбец идентификатор, то сортирую по ID
            if (e.SortResult == 0 && e.Column.Name != "ID")
            {
                e.SortResult = System.String.Compare(
                    dgv.Rows[e.RowIndex1].Cells["ID"].Value.ToString(),
                    dgv.Rows[e.RowIndex2].Cells["ID"].Value.ToString());
            }
            e.Handled = true;//обработано
        }
        // Replace this with your own population code.
        public void PopulateDataGridView()
        {
            // Add columns to the DataGridView.
            dgv.ColumnCount = 3;
            // Set the properties of the DataGridView columns.
            dgv.Columns[0].Name = "ID";
            dgv.Columns[1].Name = "Name";
            dgv.Columns[2].Name = "City";
            dgv.Columns["ID"].HeaderText = "ID";
            dgv.Columns["Name"].HeaderText = "Name";
            dgv.Columns["City"].HeaderText = "City";
            // Add rows of data to the DataGridView.
            dgv.Rows.Add(new string[] { "1", "Parker", "Seattle" });
            dgv.Rows.Add(new string[] { "2", "Parker", "New York" });
            dgv.Rows.Add(new string[] { "3", "Watson", "Seattle" });
            dgv.Rows.Add(new string[] { "4", "Jameson", "New Jersey" });
            dgv.Rows.Add(new string[] { "5", "Brock", "New York" });
            dgv.Rows.Add(new string[] { "6", "Conner", "Portland" });
            // Autosize the columns.
            dgv.AutoResizeColumns();
        }
    }
}
