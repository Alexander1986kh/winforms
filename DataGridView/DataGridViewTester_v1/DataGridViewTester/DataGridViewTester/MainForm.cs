﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridViewTester
{
    public partial class MainForm : Form
    {
        DataSet dataSet;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region Создание структуры базы данных
            dataSet = new DataSet("MyDataBase");//=аналог базы данных в памяти
            //хранит множество таблиц - объекты класса DataTable
            //некоторые таблицы - загружаются запросами select из базы данных
            //некотрые создаются - программно.
            createPlayers();
            createGames();
            #endregion

            #region Настройка внешнего вида DataGridView
            dgv.DataSource = dataSet;
            dgv.DataMember = "players";//"games";//

            dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            dgv.AllowUserToOrderColumns = true;
            dgv.RowHeadersVisible = false;
            dgv.ReadOnly = true;
            dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgv.RowTemplate.Height = 24;

            this.dgv.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_CellFormatting);
            this.dgv.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.dgv_CellParsing);

            #endregion

        }

        private void dgv_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void createGames()
        {
            DataTable table2 = new DataTable("games");
            table2.Columns.Add("GameName");
            table2.Columns.Add("CountOfPlayers");
            table2.Columns.Add("Game_Id");
            dataSet.Tables.Add(table2);
            DataRow row2 = dataSet.Tables["games"].NewRow();
            row2["Game_Id"] = "1";
            row2["GameName"] = "Крестики-нолики, Id=1";
            row2["CountOfPlayers"] = "2";
            dataSet.Tables["games"].Rows.Add(row2);
            dataSet.Tables["games"].AcceptChanges();
        }

        private void createPlayers()
        {
            DataTable table = new DataTable("players");//аналог команды create table
            //структура таблицы - по умолчанию- все столбцы типа string
            table.Columns.Add("PlayerName");
            table.Columns.Add("Age");
            table.Columns.Add("Sex");
            table.Columns.Add("Player_Id");

            dataSet.Tables.Add(table);

            DataRow row = dataSet.Tables["players"].NewRow();
            row["Player_Id"] = "1";
            row["PlayerName"] = "Иванов";
            row["Age"] = "10";
            row["Sex"] = "1";

            dataSet.Tables[0].Rows.Add(row);//или dataSet.Tables["players"].Rows.Add(row);

            DataRow row2 = dataSet.Tables["players"].NewRow();
            row2["Player_Id"] = "2";
            row2["PlayerName"] = "Петров";
            row2["Age"] = "30";
            row2["Sex"] = "1";
            dataSet.Tables[0].Rows.Add(row2);
            dataSet.Tables[0].AcceptChanges();//если ДатаСет связан с БД, то в этот момент
            //идет отправка данных в БД - фактическое выполнение insert
        }
    }
}
