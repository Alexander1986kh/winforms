﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridViewTester
{
    public partial class dgv : Form
    {
        DataSet dataSet;
        public dgv()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region Создание структуры БД
            dataSet = new DataSet("MyDataBase"); //аналог БД в памяти
                                                 //хранит множество таблиц - объекты класса DataTable
                                                 //некоторые таблицы -загружаются запросами из БД
                                                 //некоторые создаются - программно
            CreatePlayers();
            CreateGames();
            #endregion
            #region Настройка внешнего вида DataGridView
            dgv.DataSource = dataSet;
            dgv.DataMember = "players"; //"players"
            #endregion
        }

        private void CreateGames()
        {
            DataTable table2 = new DataTable("games");
            table2.Columns.Add("GameName");
            table2.Columns.Add("CountOfPlayers");
            table2.Columns.Add("Game_Id");
            dataSet.Tables.Add(table2);
            DataRow row2 = dataSet.Tables["games"].NewRow();
            row2["Game_Id"] = "1";
            row2["GameName"] = "Крестики-нолики, Id=1";
            row2["CountOfPlayers"] = "2";
            dataSet.Tables["games"].Rows.Add(row2);
            dataSet.Tables["games"].AcceptChanges();
        }

        private void CreatePlayers()
        {
            DataTable table = new DataTable("players");
            //структура таблицы - по умолчанию все столбцы типа стринг
            table.Columns.Add("PlayerName");
            table.Columns.Add("Age");
            table.Columns.Add("Sex");
            table.Columns.Add("Player_Id");
            dataSet.Tables.Add(table);
            DataRow row = dataSet.Tables["players"].NewRow();
            row["Player_Id"] = "1";
            row["PlayerName"] = "Иванов";
            row["Age"] = "10";
            row["Sex"] = "1";
            dataSet.Tables[0].Rows.Add(row); //или dataSet.Tables["players"].Rows.Add(row);
            dataSet.Tables[0].AcceptChanges(); //если DataSet связан с БД, то в этот момент, команда приводит к отправки данных в БД
                                               //фактическое выполнение Insert
        }
    }
}
