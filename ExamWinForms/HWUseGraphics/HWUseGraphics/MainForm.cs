﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HWUseGraphics
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            mypanel.Visible = true;
            mypanel.Visible = false;
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Font drawFont = new Font("Arial", 10);
            Pen mypen = new Pen(Color.Black, 2); //black pen, width 2
            Brush myBrush = Brushes.Red;
            #region Оси координат с центром (220,300)
            
            Brush myBlackBrush = Brushes.Black;//brush for drawstring
            gr.DrawString("5", drawFont, myBlackBrush, 264, 306);
            gr.DrawString("10", drawFont, myBlackBrush, 311, 306);
            gr.DrawString("15", drawFont, myBlackBrush, 361, 306);
            gr.DrawString("20", drawFont, myBlackBrush, 411, 306);
            gr.DrawString("25", drawFont, myBlackBrush, 461, 306);
            gr.DrawString("5", drawFont, myBlackBrush, 200, 240);
            gr.DrawString("10", drawFont, myBlackBrush, 195, 190);
            gr.DrawString("15", drawFont, myBlackBrush, 195, 140);
            gr.DrawString("20", drawFont, myBlackBrush, 195, 90);
            gr.DrawString("Y", drawFont, myBlackBrush, 200, 30);
            gr.DrawString("X", drawFont, myBlackBrush, 520, 306);
            gr.DrawLine(mypen,220,30,220, 300);//OY

            gr.DrawLine(mypen, 220, 30, 216, 40);
            gr.DrawLine(mypen, 220, 30, 224, 40);
            gr.DrawLine(mypen, 220, 300, 530,300);//OX

            gr.DrawLine(mypen, 520, 296, 530, 300);
            gr.DrawLine(mypen, 520, 304, 530, 300);//стрелочки

            gr.DrawLine(mypen, 220, 250, 216, 250);
            gr.DrawLine(mypen, 220, 200, 216, 200);
            gr.DrawLine(mypen, 220, 150, 216, 150);
            gr.DrawLine(mypen, 220, 100, 216, 100);//отрезки на 0Y

            gr.DrawLine(mypen, 270, 300, 270, 304);
            gr.DrawLine(mypen, 320, 300, 320, 304);
            gr.DrawLine(mypen, 370, 300, 370, 304);
            gr.DrawLine(mypen, 420, 300, 420, 304);
            gr.DrawLine(mypen, 470, 300, 470, 304);//отрезки на 0X
            #endregion
            if (cmbExNumber.SelectedItem.ToString() == "Задание 21")
            { 
                gr.DrawEllipse(mypen, 320, 150, 100, 100);
                gr.FillEllipse(myBrush, 320, 150, 100, 100);//круг с диаметром 100 центром(320,200)
            }
          
            if (cmbExNumber.SelectedItem.ToString() == "Задание 22")
            {
                gr.DrawRectangle(mypen, 270, 150, 200, 50);
                gr.FillRectangle(myBrush, 270, 150, 200, 50);
            }

            if (cmbExNumber.SelectedItem.ToString() == "Задание 23")
            {
                gr.DrawRectangle(mypen, 340, 100, 80, 200);
                gr.FillRectangle(myBrush, 340, 100, 80, 200);
                gr.DrawEllipse(mypen, 320, 150, 120, 120);
                gr.FillEllipse(Brushes.Green, 320, 150, 120, 120);
                gr.DrawRectangle(mypen, 340, 100, 80, 200);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (txtBoxX.Text == "" || txtBoxY.Text == "")
            {
                MessageBox.Show("Введите координаты точки");
            }
            else
            {
                Graphics gr = Graphics.FromHwnd(this.Handle);
                Pen mypen = new Pen(Color.Black, 2);
                int x = (Convert.ToInt32(txtBoxX.Text) * 10) + 220;
                int y = 300 - (Convert.ToInt32(txtBoxY.Text) * 10);

                gr.DrawLine(mypen, x - 1, y - 1, x + 1, y + 1);
            }
        }
    }
}
