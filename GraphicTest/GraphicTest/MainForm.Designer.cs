﻿namespace GraphicTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDrawRect = new System.Windows.Forms.Button();
            this.btnDrawEllipse = new System.Windows.Forms.Button();
            this.btnDrawCircle = new System.Windows.Forms.Button();
            this.btnTestDLL = new System.Windows.Forms.Button();
            this.btnDrawTriangle = new System.Windows.Forms.Button();
            this.pBox = new System.Windows.Forms.PictureBox();
            this.btnOpenImage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDrawRect
            // 
            this.btnDrawRect.Location = new System.Drawing.Point(12, 417);
            this.btnDrawRect.Name = "btnDrawRect";
            this.btnDrawRect.Size = new System.Drawing.Size(99, 23);
            this.btnDrawRect.TabIndex = 0;
            this.btnDrawRect.Text = "Прямоугольник";
            this.btnDrawRect.UseVisualStyleBackColor = true;
            this.btnDrawRect.Click += new System.EventHandler(this.btnDrawRect_Click);
            // 
            // btnDrawEllipse
            // 
            this.btnDrawEllipse.Location = new System.Drawing.Point(117, 417);
            this.btnDrawEllipse.Name = "btnDrawEllipse";
            this.btnDrawEllipse.Size = new System.Drawing.Size(75, 23);
            this.btnDrawEllipse.TabIndex = 1;
            this.btnDrawEllipse.Text = "Эллипс";
            this.btnDrawEllipse.UseVisualStyleBackColor = true;
            this.btnDrawEllipse.Click += new System.EventHandler(this.btnDrawEllipse_Click);
            // 
            // btnDrawCircle
            // 
            this.btnDrawCircle.Location = new System.Drawing.Point(198, 417);
            this.btnDrawCircle.Name = "btnDrawCircle";
            this.btnDrawCircle.Size = new System.Drawing.Size(75, 23);
            this.btnDrawCircle.TabIndex = 2;
            this.btnDrawCircle.Text = "Круг";
            this.btnDrawCircle.UseVisualStyleBackColor = true;
            this.btnDrawCircle.Click += new System.EventHandler(this.btnDrawCircle_Click);
            // 
            // btnTestDLL
            // 
            this.btnTestDLL.Location = new System.Drawing.Point(360, 417);
            this.btnTestDLL.Name = "btnTestDLL";
            this.btnTestDLL.Size = new System.Drawing.Size(75, 23);
            this.btnTestDLL.TabIndex = 3;
            this.btnTestDLL.Text = "TestDLL";
            this.btnTestDLL.UseVisualStyleBackColor = true;
            this.btnTestDLL.Click += new System.EventHandler(this.btnTestDLL_Click);
            // 
            // btnDrawTriangle
            // 
            this.btnDrawTriangle.Location = new System.Drawing.Point(279, 417);
            this.btnDrawTriangle.Name = "btnDrawTriangle";
            this.btnDrawTriangle.Size = new System.Drawing.Size(75, 23);
            this.btnDrawTriangle.TabIndex = 4;
            this.btnDrawTriangle.Text = "Треуголник";
            this.btnDrawTriangle.UseVisualStyleBackColor = true;
            this.btnDrawTriangle.Click += new System.EventHandler(this.btnDrawTriangle_Click);
            // 
            // pBox
            // 
            this.pBox.Image = global::GraphicTest.Properties.Resources.myresult;
            this.pBox.Location = new System.Drawing.Point(441, 12);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(340, 399);
            this.pBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBox.TabIndex = 5;
            this.pBox.TabStop = false;
            // 
            // btnOpenImage
            // 
            this.btnOpenImage.Location = new System.Drawing.Point(576, 417);
            this.btnOpenImage.Name = "btnOpenImage";
            this.btnOpenImage.Size = new System.Drawing.Size(75, 23);
            this.btnOpenImage.TabIndex = 6;
            this.btnOpenImage.Text = "Open";
            this.btnOpenImage.UseVisualStyleBackColor = true;
            this.btnOpenImage.Click += new System.EventHandler(this.btnOpenImage_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 452);
            this.Controls.Add(this.btnOpenImage);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.btnDrawTriangle);
            this.Controls.Add(this.btnTestDLL);
            this.Controls.Add(this.btnDrawCircle);
            this.Controls.Add(this.btnDrawEllipse);
            this.Controls.Add(this.btnDrawRect);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "Тест графики";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDrawRect;
        private System.Windows.Forms.Button btnDrawEllipse;
        private System.Windows.Forms.Button btnDrawCircle;
        private System.Windows.Forms.Button btnTestDLL;
        private System.Windows.Forms.Button btnDrawTriangle;
        private System.Windows.Forms.PictureBox pBox;
        private System.Windows.Forms.Button btnOpenImage;
    }
}

