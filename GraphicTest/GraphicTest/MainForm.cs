﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicTest
{
    public partial class MainForm : Form //Наследование структуры (класс разбит на несколько файлов (partial - ключевое слово))
    {
        private int xCircle = 100;
        private int yCircle = 100;
        public MainForm()
        {
            InitializeComponent();
            btnDrawRect.Text = "Прямоугольник";
            btnDrawEllipse.Text = "Эллипс";
            btnDrawCircle.Text = "Круг";
        }
        private void btnDrawRect_Click(object sender, EventArgs e)
        {
            //Получаем графический контекст == Холст, на котором можно рисовать
            //Определиться с контуром - Pen
            //Определиться с кистью - Brush
            //вызываем из контекста необходимую функцию
            Graphics graphics = Graphics.FromHwnd(this.Handle); //Аналог имени окна в Windows (номер объекта внутри Windows)
            Pen myPen = Pens.Red; //Стандартный контур (цвет)
            Brush myBrush = Brushes.BlueViolet;
            int x = 100;
            int y = 150;
            int width = 50;
            int height = 25;
            graphics.DrawRectangle(myPen, x, y, width, height);
        }
        private void btnDrawEllipse_Click(object sender, EventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(this.Handle); //Аналог имени окна в Windows (номер объекта внутри Windows)
            Pen myPen = Pens.Green; //Стандартный контур (цвет)
            Brush myBrush = Brushes.Coral;
            int x = 100;
            int y = 150;
            int width = 50;
            int height = 25;
            graphics.DrawEllipse(myPen, x, y, width, height);
        }
        private void btnDrawCircle_Click(object sender, EventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(this.Handle); //Аналог имени окна в Windows (номер объекта внутри Windows)
            //Pen myPen = Pens.Blue; //Стандартный контур (цвет)
            Brush myBrush = Brushes.Blue;
            int x = 112;
            int y = 150;
            //int width = 50;
            int height = 25;
            graphics.FillEllipse(myBrush, x, y, height, height);
        }
        private void btnTestDLL_Click(object sender, EventArgs e)
        {
            //String msg = Itstep.Kharkov.Tester.MyObjects.MyMessager.formatMSG("Вот так!!!");
          //  MessageBox.Show(msg, "Я dll, н..!");
        }
        private void btnDrawTriangle_Click(object sender, EventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            //Graphics.FromHwnd - получение холста: поверхность, на которой выводится изображение
            Point p1 = new Point(260, 25);
            Point p2 = new Point(325, 225);
            Point p3 = new Point(260, 225);
            Point[] points = new Point[3];
            points[0] = p1;
            points[1] = p2;
            points[2] = p3;
            Color myColor = Color.FromArgb(70, 255, 230, 100);
            Pen myPen = new Pen(myColor, 4); //контур фигур
            Brush myBrush = Brushes.Gray; //заливка фигур
            gr.DrawPolygon(myPen, points);
            gr.FillPolygon(myBrush, points);
        }
        private void btnOpenImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open Image";
            dlg.Filter = "bmp files (*.bmp)|*.bmp|jpg files (*.jpg)|*.jpg|All files (*.*)|*.*";
            if(dlg.ShowDialog() == DialogResult.OK)
            {
                pBox.Image = new Bitmap(dlg.FileName);
            }
            dlg.Dispose(); //обращение к ресурсам унаследованного кода
            //выполняем из-за OpenFileDialog - обёртка над устаревшими механизмами WinApi
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            //конструктор отработал и окно полностью загружено
            //выполняется один раз, как и конструктор
            Graphics gr = Graphics.FromImage(pBox.Image);
            gr.DrawEllipse(Pens.Purple, 10, 10, 25, 25);
            gr.FillEllipse(Brushes.Purple, 10, 10, 25, 25);
            pBox.Image.Save("myresult.bmp");
        }
        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Gray;
            Brush myBrush = Brushes.Black;
            gr.DrawEllipse(myPen, xCircle, yCircle, 25, 25); //x и y - левый верхний угол, далее ширина и высота прямоугольника
            //myPen - контур, myBrush - кисть
            gr.FillEllipse(myBrush, xCircle, yCircle, 25, 25);
            //keyPreview = true!!!
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if(ch == 'd')
            {
                xCircle += 10;
                this.Invalidate(); //команда перерисоваться окну (будет вызван Paint)
            }
            if (ch == 'a')
            {
                xCircle -= 10;
                this.Invalidate(); //команда перерисоваться окну (будет вызван Paint)
            }
            if (ch == 'w')
            {
                yCircle -= 10;
                this.Invalidate(); //команда перерисоваться окну (будет вызван Paint)
            }
            if (ch == 's')
            {
                yCircle += 10;
                this.Invalidate(); //команда перерисоваться окну (будет вызван Paint)
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Left)
            {
                xCircle -= 10;
                this.Invalidate();
                return true;
            }
            else if (keyData == Keys.Right)
            {
                xCircle += 10;
                this.Invalidate();
                return true;
            }
            else return false;
        }
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            this.Text="Нажата клавиша" + e.KeyCode;
            if (e.KeyCode == Keys.Left)
            {
                xCircle += 10;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.Right)
            {
                xCircle -= 10;
                this.Invalidate();  
            }
           
        }
    }
}