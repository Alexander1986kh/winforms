﻿namespace HWUseGraphics
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTask = new System.Windows.Forms.Label();
            this.cmbExNumber = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblPointCoords = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.txtBoxY = new System.Windows.Forms.TextBox();
            this.txtBoxX = new System.Windows.Forms.TextBox();
            this.btnView = new System.Windows.Forms.Button();
            this.mypanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lblTask
            // 
            this.lblTask.AutoSize = true;
            this.lblTask.Location = new System.Drawing.Point(31, 22);
            this.lblTask.Name = "lblTask";
            this.lblTask.Size = new System.Drawing.Size(34, 13);
            this.lblTask.TabIndex = 0;
            this.lblTask.Text = "Task:";
            // 
            // cmbExNumber
            // 
            this.cmbExNumber.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbExNumber.FormattingEnabled = true;
            this.cmbExNumber.Items.AddRange(new object[] {
            "Задание 21",
            "Задание 22",
            "Задание 23"});
            this.cmbExNumber.Location = new System.Drawing.Point(12, 49);
            this.cmbExNumber.Name = "cmbExNumber";
            this.cmbExNumber.Size = new System.Drawing.Size(121, 21);
            this.cmbExNumber.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(15, 143);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(150, 28);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblPointCoords
            // 
            this.lblPointCoords.AutoSize = true;
            this.lblPointCoords.Location = new System.Drawing.Point(98, 181);
            this.lblPointCoords.Name = "lblPointCoords";
            this.lblPointCoords.Size = new System.Drawing.Size(69, 13);
            this.lblPointCoords.TabIndex = 3;
            this.lblPointCoords.Text = "Point coords:";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(12, 262);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(14, 13);
            this.lblY.TabIndex = 4;
            this.lblY.Text = "Y";
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(12, 221);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(14, 13);
            this.lblX.TabIndex = 5;
            this.lblX.Text = "X";
            // 
            // txtBoxY
            // 
            this.txtBoxY.Location = new System.Drawing.Point(65, 259);
            this.txtBoxY.Name = "txtBoxY";
            this.txtBoxY.Size = new System.Drawing.Size(100, 20);
            this.txtBoxY.TabIndex = 6;
            // 
            // txtBoxX
            // 
            this.txtBoxX.Location = new System.Drawing.Point(65, 218);
            this.txtBoxX.Name = "txtBoxX";
            this.txtBoxX.Size = new System.Drawing.Size(100, 20);
            this.txtBoxX.TabIndex = 7;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(65, 289);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(100, 23);
            this.btnView.TabIndex = 8;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // mypanel
            // 
            this.mypanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.mypanel.Location = new System.Drawing.Point(219, 3);
            this.mypanel.Name = "mypanel";
            this.mypanel.Size = new System.Drawing.Size(541, 309);
            this.mypanel.TabIndex = 9;
            this.mypanel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 324);
            this.Controls.Add(this.mypanel);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.txtBoxX);
            this.Controls.Add(this.txtBoxY);
            this.Controls.Add(this.lblX);
            this.Controls.Add(this.lblY);
            this.Controls.Add(this.lblPointCoords);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.cmbExNumber);
            this.Controls.Add(this.lblTask);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTask;
        private System.Windows.Forms.ComboBox cmbExNumber;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblPointCoords;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.TextBox txtBoxY;
        private System.Windows.Forms.TextBox txtBoxX;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Panel mypanel;
    }
}

