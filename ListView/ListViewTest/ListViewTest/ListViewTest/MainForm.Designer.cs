﻿namespace ListViewTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewQuarters = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.listViewMonths = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.lvEmployees = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.show_selected = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listViewQuarters
            // 
            this.listViewQuarters.Location = new System.Drawing.Point(12, 47);
            this.listViewQuarters.Name = "listViewQuarters";
            this.listViewQuarters.Size = new System.Drawing.Size(181, 133);
            this.listViewQuarters.TabIndex = 0;
            this.listViewQuarters.UseCompatibleStateImageBehavior = false;
            this.listViewQuarters.SelectedIndexChanged += new System.EventHandler(this.listViewQuarters_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "список кварталов:";
            // 
            // listViewMonths
            // 
            this.listViewMonths.Location = new System.Drawing.Point(199, 47);
            this.listViewMonths.Name = "listViewMonths";
            this.listViewMonths.Size = new System.Drawing.Size(121, 179);
            this.listViewMonths.TabIndex = 2;
            this.listViewMonths.UseCompatibleStateImageBehavior = false;
            this.listViewMonths.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewMonths_ColumnClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(196, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "месяцы";
            // 
            // lvEmployees
            // 
            this.lvEmployees.Location = new System.Drawing.Point(422, 44);
            this.lvEmployees.Name = "lvEmployees";
            this.lvEmployees.Size = new System.Drawing.Size(127, 203);
            this.lvEmployees.TabIndex = 4;
            this.lvEmployees.UseCompatibleStateImageBehavior = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(429, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Список сотрудников";
            // 
            // show_selected
            // 
            this.show_selected.Location = new System.Drawing.Point(422, 278);
            this.show_selected.Name = "show_selected";
            this.show_selected.Size = new System.Drawing.Size(75, 23);
            this.show_selected.TabIndex = 6;
            this.show_selected.Text = "Показать выбранное";
            this.show_selected.UseVisualStyleBackColor = true;
            this.show_selected.Click += new System.EventHandler(this.show_selected_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 347);
            this.Controls.Add(this.show_selected);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lvEmployees);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listViewMonths);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listViewQuarters);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Работа со списками";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewQuarters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listViewMonths;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvEmployees;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button show_selected;
    }
}

