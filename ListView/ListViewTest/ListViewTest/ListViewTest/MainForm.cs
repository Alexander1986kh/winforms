﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;

namespace ListViewTest
{
    public partial class MainForm : Form
    {
        private string[] first = new string[3] { "January", "February", "March" };
        private string[] second = new string[3] { "April", "May", "June" };
        private string[] third = new string[3] { "July", "August", "September" };
        private string[] fourth = new string[3] { "October", "November", "December" };
        public MainForm()
        {
            InitializeComponent();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            listViewQuarters.View = View.Details;//внешний вид - таблица
            listViewQuarters.GridLines = true;//строки прорисовывать
            listViewQuarters.FullRowSelect = true;//отображать выделенную строку
            listViewQuarters.MultiSelect = true;//разрешать выделять несколько строк
            //создаю содержимое первого списка
            listViewQuarters.Columns.Add("Quarters", 120);//создаю одну колонку с заголовком 'Quarters'
            //содержимое списка - перечень строк
            listViewQuarters.Items.Add("The First Quarter");
            listViewQuarters.Items.Add("The Second Quarter");
            listViewQuarters.Items.Add("The Third Quarter");
            listViewQuarters.Items.Add("The Fourth Quarter");
            //задание внешнего вида второго списка
            listViewMonths.View = View.Details;
            listViewMonths.GridLines = true;
            listViewMonths.FullRowSelect = true;
            listViewMonths.MultiSelect = true;
            listViewMonths.Columns.Add("Months", 100);


            lvEmployees.View = View.Details;
            lvEmployees.GridLines = true;
            lvEmployees.FullRowSelect = true;
            lvEmployees.CheckBoxes = true;//каждая строка может быть отмечена
            //Add column header
            lvEmployees.Columns.Add("ID", 50);//0
            lvEmployees.Columns.Add("Phone", 100);//1
            lvEmployees.Columns.Add("Last Name", 120);//2
            lvEmployees.Columns.Add("First Name", 140);//3
            #region Add Items to lvEmployees
            string[] arr = new string[4];

            arr[0] = "5001";
            arr[1] = "159-224-77-199";
            arr[2] = "Петров";
            arr[3] = "Иван";
            ListViewItem itm = new ListViewItem(arr);
            itm.Checked = true;//отмечена строка( если lvEmployees.CheckBoxes = true;)
            lvEmployees.Items.Add(itm);
          
            
            arr[0] = "5003241";
            arr[1] = "157-199";
            arr[2] = "Петр";
            arr[3] = "Иванов";
            itm = new ListViewItem(arr);
            itm.Checked = false;
            itm.ForeColor = Color.Red;
            itm.BackColor = Color.Green;
            lvEmployees.Items[0].Selected = true;
            lvEmployees.Items.Add(itm);
            #endregion
        }
        private void listViewQuarters_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int z = 0; z < listViewQuarters.SelectedItems.Count; z++)
            {
                listViewMonths.Items.Clear();
                for (int i = 0; i < listViewQuarters.SelectedIndices.Count; i++)
                {
                    int index = listViewQuarters.SelectedIndices[i];
                    if (0 == index)
                    {
                        for (int j = 0; j < first.Length; j++)
                        {
                            listViewMonths.Items.Add(first[j]);
                        }
                    }
                    if (1 == index)
                    {
                        for (int j = 0; j < second.Length; j++)
                        {
                            listViewMonths.Items.Add(second[j]);
                        }
                    }
                    if (2 == index)
                    {
                        for (int j = 0; j < third.Length; j++)
                        {
                            listViewMonths.Items.Add(third[j]);
                        }
                    }
                    if (3 == index)
                    {
                        for (int j = 0; j < fourth.Length; j++)
                        {
                            listViewMonths.Items.Add(fourth[j]);
                        }
                    }
                }
            }
        }

        private void listViewMonths_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ArrayList sortlist = new ArrayList();//using System.Collections;
            for (int i = 0; i < listViewMonths.Items.Count; i++)
            {
                sortlist.Add(listViewMonths.Items[i].Text);
            }
            sortlist.Sort();
            listViewMonths.Items.Clear();
            foreach (string i in sortlist)
            {
                listViewMonths.Items.Add(i);
            }
        }

        private void show_selected_Click(object sender, EventArgs e)
        {
            if(lvEmployees.SelectedItems.Count>0) {
                string id = lvEmployees.SelectedItems[0].SubItems[0].Text;
                string phone = lvEmployees.SelectedItems[0].SubItems[1].Text;
                string last_name = lvEmployees.SelectedItems[0].SubItems[2].Text;
                string info = String.Format("{2}--{1}--{0}", id, phone,last_name);
                MessageBox.Show("Выбрали -" + info);
            }
        }
    }
}
