﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NorthWindViewtester
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region Соеденение с базой
            DataSet mydataset = new DataSet();
            string connectionString = @"Data Source=DESKTOP-C6D4823\SQLEXPRESS;Initial Catalog=C:\DEVMED\SAMPLES\WPF\DYNAMICLISTVIEW\DATA\NORTHWIND.MDF;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("select * from [dbo].Employees", con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    //Создаем адаптер для загрузки данных из базы по запросу
                    adapter.Fill(mydataset, "MyEmployees");
                    //Заполняем DataSet данными из адаптера, 
                    //Создав в нем таблицу MyEmployees
                }
                using (SqlCommand command = new SqlCommand("select * from [dbo].Categories", con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    //Создаем адаптер для загрузки данных из базы по запросу
                    adapter.Fill(mydataset, "MyCategories");
                    //Заполняем DataSet данными из адаптера, 
                    //Создав в нем таблицу MyEmployees
                }

                List<Customer> myCustomerList = new List<Customer>();
                using (SqlCommand command = new SqlCommand("select * from [dbo].Categories", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int v0 = reader.GetInt32(0);
                        string v1 = reader.GetString(1);
                        string v2 = reader.GetString(2);
                        string v3 = reader.GetString(3);
                        string v4 = reader.GetString(4);
                        string v5 = reader.GetString(5);
                        string v6 = reader.GetString(6);
                        string v7 = reader.GetString(7);
                        string v8 = reader.GetString(8);
                        string v9 = reader.GetString(9);
                        string v10 = reader.GetString(10);
                        Customer newCustomer = new Customer(v0,
                            v1, v2, v3, v4, v5, v6, v7, v8, v9, v10);
                        myCustomerList.Add(newCustomer);                 
                    }
                }
            }
            #endregion
            #region DataGridViewEmployee
            dgvEmployees.DataSource = mydataset.Tables["MyEmployees"];
            //dgv-интерфейсных данных из базы данных
            #endregion
            dgvCustomers.
        }
    }
}
