﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace MyPassportData
{
    public partial class PassportData : UserControl
    {
        
        public PassportData()
        {
            InitializeComponent();
        }

        private void PassportData_Load(object sender, EventArgs e)
        {
            string[] mas = new string[]{ "00", "11",
                "22", "33", "44", "55", "66", "77", "88", "99" };
            cmbSeries.Items.AddRange(mas);
        }
        #region свойтва
        [Browsable(true)]//в свойствах компонента будет доступно значение этого свойства 
        public override string Text
        {
            get
            {
                return txtNumber.Text;
            }

            set
            {               
                txtNumber.Text = value;
            }
        }
        public string PassportSeries
        { get
            {if (cmbSeries.SelectedItem != null)
                    return cmbSeries.SelectedItem.ToString();
                else return "";
            }
        }
        public DateTime DateIssue {
            get { return dateIssue.Value; }
            set { dateIssue.Value = value; }
        }
        #endregion

        private void txtNumber_Leave(object sender, EventArgs e)
            //Потеря фокуса этим элементом
        {
            Regex regex = new Regex(@"^\d{6}$");
            if (regex.IsMatch(txtNumber.Text) == false)
            {
                MessageBox.Show("Неправильный формат номера паспорта(6 цыфр)");
            }
                    }
    }
}
