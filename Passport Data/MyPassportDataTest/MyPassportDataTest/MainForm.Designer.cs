﻿namespace MyPassportDataTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PassportData = new MyPassportData.PassportData();
            this.SuspendLayout();
            // 
            // PassportData
            // 
            this.PassportData.DateIssue = new System.DateTime(2017, 9, 19, 9, 55, 25, 168);
            this.PassportData.Location = new System.Drawing.Point(12, 12);
            this.PassportData.Name = "PassportData";
            this.PassportData.Size = new System.Drawing.Size(758, 63);
            this.PassportData.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 289);
            this.Controls.Add(this.PassportData);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MyPassportData.PassportData PassportData;
    }
}

