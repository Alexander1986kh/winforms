﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeMVC
{
//Модель игры- обертка над матрицей
//Ответственность : поиск линий одинаковых значений
    class TicTacToeModel//
    {
        private string[,] matrix;
        public static readonly string XValue = "X";
        public static readonly string OValue = "0";
        public static readonly string EmptyValue = "-";//статическое поле, только для чтения
        public TicTacToeModel()
        {
            matrix = new string[3, 3] {
                                        { EmptyValue , EmptyValue , EmptyValue },
                                        { EmptyValue , EmptyValue , EmptyValue },
                                        { EmptyValue , EmptyValue , EmptyValue }
             };
        }
         public bool haveLine(string value) {
            if (matrix[0, 0] == value && matrix[0, 1] == value && matrix[0, 2] == value ||
                matrix[1, 0] == value && matrix[1, 1] == value && matrix[1, 2] == value ||
                matrix[2, 0] == value && matrix[2, 1] == value && matrix[2, 2] == value ||
                matrix[0, 0] == value && matrix[1, 0] == value && matrix[2, 0] == value ||
                matrix[0, 1] == value && matrix[1, 1] == value && matrix[2, 1] == value ||
                matrix[0, 2] == value && matrix[1, 2] == value && matrix[2, 2] == value ||
                matrix[0, 0] == value && matrix[1, 1] == value && matrix[2, 2] == value ||
                matrix[2, 0] == value && matrix[1, 1] == value && matrix[0, 2] == value
                )

                return true;
            else return false;
         }

     }
}

