﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeMVC
{
    public partial class View1 : Form
    {
        private TicTacToeController controller;
        public Button[,] btn_mas=new Button[3,3];
        public View1()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            createView();
        }

        private void createView()
        {
           
        }

        private void View1_Load(object sender, EventArgs e)
        {
            
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                { 
                    btn_mas[i, j] = new Button();
                
                    this.btn_mas[i, j].Size = new System.Drawing.Size(40, 40);
                    this.btn_mas[i, j].ForeColor = System.Drawing.Color.DarkRed;
                    this.btn_mas[i, j].Location = new System.Drawing.Point(40 + i * 50, 40 + j * 50);
                 
                    this.btn_mas[i, j].Name = String.Format("b{1}{0}", i + 1, j + 1);
                    this.btn_mas[i, j].TabIndex = 0;
                    this.btn_mas[i, j].Text = TicTacToeModel.EmptyValue;
                    this.btn_mas[i, j].Click += new EventHandler(btn_click);//вешаем обработчик события на Button
                    this.Controls.Add(this.btn_mas[i, j]);

                }

            }
        }
        public int count;
        public int iposition;
        public int jposition;
        private void btn_click(object sender, EventArgs e)
        {// Выполняем проверку- объект Sender является ли экземпляром Button
            if (sender is Button)
            {
                Button btn = sender as Button;
                //в случае успешного преобразования получаем объект Button иначе null
                if (btn != null)
                {
                    char[] parse_btn_name= new char[btn.Name.Length];
                    parse_btn_name = btn.Name.ToCharArray();
                    iposition = (int)(parse_btn_name[btn.Name.Length - 2] - '0');
                    jposition = (int)(parse_btn_name[btn.Name.Length - 1]-'0');

                    btn.Text = iposition.ToString();
                    

                }
            }
            
        }
    }
}
