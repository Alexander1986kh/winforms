﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToeMVC
{
    /// <summary>
    /// Класс - управленец - контроллер. Отвечает: за логику игры (чередование ходов и поиск победителя). Передает данные из View в Model и наоборот.
    /// </summary>
    public sealed class TicTacToeController
    {
        private TicTacToeModel model = new TicTacToeModel();
        protected TicTacToeController()
        {

        }
        View1 currentView;
        public TicTacToeController(View1 view)
        {
            this.currentView = view;
        }

    }
}
