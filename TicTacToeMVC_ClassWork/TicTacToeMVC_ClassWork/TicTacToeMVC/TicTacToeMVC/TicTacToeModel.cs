﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToeMVC
{
    /// <summary>
    /// Модель игры - обертка на матрицей. Ответственность: поиск "линий" одинаковых значений.
    /// </summary>
    public class TicTacToeModel
    {
        private string[,] matrix;
        string XValue = "X";
        string OValue = "0";
        string EmptyValue = "-";
        public TicTacToeModel()
        {
            matrix = new string[3, 3] { 
                                       { EmptyValue, EmptyValue, EmptyValue },
                                       { EmptyValue, EmptyValue, EmptyValue },
                                       { EmptyValue, EmptyValue, EmptyValue }
                                      };
        }
        public bool haveLine(string value)
        {
            return false;
        }
    }
}
