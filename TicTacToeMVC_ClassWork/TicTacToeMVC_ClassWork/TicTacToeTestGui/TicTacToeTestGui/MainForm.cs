﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeTestGui
{
    public partial class MainForm : Form
    {
        private Button btn31;
        private Button[] btnLine3;
        public MainForm()
        {
            InitializeComponent();
            MyInitializeComponet();
        }
        private void MyInitializeComponet()
        {
            #region Пример настройки одного компонента
            /*
            this.btn31 = new System.Windows.Forms.Button();
            //создал компонент в памяти
            //задал внешний вид
            this.btn31.Location = new System.Drawing.Point(19, 81 + 47);
            this.btn31.Name = "btn31";
            this.btn31.Size = new System.Drawing.Size(75, 47);
            this.btn31.TabIndex = 3;
            this.btn31.Text = "31";
            this.btn31.UseVisualStyleBackColor = true;
            this.btn31.Click += new System.EventHandler(this.btn31_Click);
            //поместил в контейнер, который будет отвечать за отображение
            this.Controls.Add(this.btn31);
            */
            #endregion
            btnLine3 = new Button[3];
            int x = 19;
            int y = 81;
            int width = 75;
            int height = 47;
            y = y + height;
            for(int i = 0; i < btnLine3.Length;i++)
            {
                btnLine3[i] = new System.Windows.Forms.Button();
                //создал компонент в памяти
                //задал внешний вид
                btnLine3[i].Location = new System.Drawing.Point(x, y);
                btnLine3[i].Name = "btn3"+(i+1).ToString();
                btnLine3[i].Size = new System.Drawing.Size(width, height);
                btnLine3[i].TabIndex = 3;
                btnLine3[i].Text = "3" + (i + 1).ToString();
                btnLine3[i].UseVisualStyleBackColor = true;
                btnLine3[i].Click += new System.EventHandler(this.btn31_Click);
                //поместил в контейнер, который будет отвечать за отображение
                this.Controls.Add(btnLine3[i]);
                x = x + width;
            }
        }
        private void btn11_Click(object sender, EventArgs e)
        {
            btn11.Text = "Clicked!";
        }

        private void btn12_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;//явное преобразование типа в синтаксисе семейства си
            btn.Text = "Clicked";
        }

        private void btn31_Click(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    btn.Text = "Clicked:"+btn.Name;
                }
            }
        }
    }
}
