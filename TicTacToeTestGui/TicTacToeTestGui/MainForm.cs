﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeTestGui
{
    public partial class MainForm : Form
    {
        private Button btn31;
        private Button[] btnLine3;
        public MainForm()
        {
            InitializeComponent();
            MyInitializeComponent();
        }

        private void MyInitializeComponent()
        {
            #region Добавление и настройка одного компонента
            /*
            // 
            // btn31
            // 
            btn31 = new Button();
            btn31.Location = new System.Drawing.Point(13, 83 + 70);
            btn31.Name = "btn31";
            btn31.Size = new System.Drawing.Size(75, 64);
            btn31.TabIndex = 6;
            btn31.Text = "31";
            btn31.UseVisualStyleBackColor = true;
            btn31.Click += new System.EventHandler(this.btn13_Click);
            this.Controls.Add(this.btn31); //добавляет компонент в контейнер
                                           //контейнеры отвечают за отображение всех принадлежащих компонентов
                                           */
            #endregion
            btnLine3 = new Button[3];
            int x = 13;
            int y = 83;
            int width = 75;
            int height = 64;
            y = y + height + 6;
            for (int i = 0; i < btnLine3.Length; i++)
            {           
                btnLine3[i] = new Button();
                btnLine3[i].Location = new System.Drawing.Point(x, y);
                btnLine3[i].Name = "btn3" + (i + 1).ToString();
                btnLine3[i].Size = new System.Drawing.Size(width, height);
                btnLine3[i].TabIndex = 6;
                btnLine3[i].Text = "3" + (i + 1).ToString();
                btnLine3[i].UseVisualStyleBackColor = true;
                btnLine3[i].Click += new System.EventHandler(this.btn13_Click);
                this.Controls.Add(this.btnLine3[i]);
                x = x + width + 6;
            }
        }

        private void btn11_Click(object sender, EventArgs e)
        {
            btn11.Text = "Clicked!";
        }

        private void btn12_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender; //явное преобразование типа в синтаксисе семейства C
            btn.Text = "Clicked!";
        }

        private void btn13_Click(object sender, EventArgs e)
        {
            if(sender is Button)
            {
                Button btn = sender as Button;
                if(btn != null)
                {
                    btn.Text = "Clicked: " + btn.Name;
                }              
            }
        }
    }
}
