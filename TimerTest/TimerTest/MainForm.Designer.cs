﻿namespace TimerTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTime = new System.Windows.Forms.Label();
            this.Timer_time_now = new System.Windows.Forms.Timer(this.components);
            this.btnStop = new System.Windows.Forms.Button();
            this.lblMes = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.btnTimer2 = new System.Windows.Forms.Button();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.inheritedTimer4 = new System.Windows.Forms.Timer(this.components);
            this.Timer_for_ellipse = new System.Windows.Forms.Timer(this.components);
            this.btn_start_ellipse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTime.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblTime.Location = new System.Drawing.Point(87, 92);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(61, 23);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "label1";
            // 
            // Timer_time_now
            // 
            this.Timer_time_now.Enabled = true;
            this.Timer_time_now.Interval = 1000;
            this.Timer_time_now.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(73, 194);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            this.btnStop.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnStop_MouseClick);
            // 
            // lblMes
            // 
            this.lblMes.AutoSize = true;
            this.lblMes.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMes.Location = new System.Drawing.Point(41, 33);
            this.lblMes.Name = "lblMes";
            this.lblMes.Size = new System.Drawing.Size(73, 22);
            this.lblMes.TabIndex = 2;
            this.lblMes.Text = "Привет";
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // btnTimer2
            // 
            this.btnTimer2.Location = new System.Drawing.Point(183, 194);
            this.btnTimer2.Name = "btnTimer2";
            this.btnTimer2.Size = new System.Drawing.Size(75, 23);
            this.btnTimer2.TabIndex = 3;
            this.btnTimer2.Text = "Побежали?";
            this.btnTimer2.UseVisualStyleBackColor = true;
            this.btnTimer2.Click += new System.EventHandler(this.btnTimer2_Click);
            // 
            // timer3
            // 
            this.timer3.Interval = 2;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // inheritedTimer4
            // 
            this.inheritedTimer4.Enabled = true;
            this.inheritedTimer4.Interval = 10;
            this.inheritedTimer4.Tick += new System.EventHandler(this.inheritedTimer4_Tick);
            // 
            // Timer_for_ellipse
            // 
            this.Timer_for_ellipse.Interval = 10;
            this.Timer_for_ellipse.Tick += new System.EventHandler(this.Timer_for_ellipse_Tick);
            // 
            // btn_start_ellipse
            // 
            this.btn_start_ellipse.Location = new System.Drawing.Point(628, 193);
            this.btn_start_ellipse.Name = "btn_start_ellipse";
            this.btn_start_ellipse.Size = new System.Drawing.Size(75, 23);
            this.btn_start_ellipse.TabIndex = 5;
            this.btn_start_ellipse.Text = "Start ellipse";
            this.btn_start_ellipse.UseVisualStyleBackColor = true;
            this.btn_start_ellipse.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 285);
            this.Controls.Add(this.btn_start_ellipse);
            this.Controls.Add(this.btnTimer2);
            this.Controls.Add(this.lblMes);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.lblTime);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Timer Timer_time_now;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblMes;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button btnTimer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer inheritedTimer4;
        private System.Windows.Forms.Timer Timer_for_ellipse;
        private System.Windows.Forms.Button btn_start_ellipse;
    }
}

