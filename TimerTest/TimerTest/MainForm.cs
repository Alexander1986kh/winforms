﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimerTest
{
    public partial class MainForm : Form
    //ДЗ
    /*
     на кнопке стоп запустить отсчет времени 9.8.7....,0,
     при достижении нуля показать на ней надпись Стопи сделать ее кликабельной-активной
     ДЗ2
     Добавить под движением метки движение красного круга
     ДЗ3Три кружка пдают с разной скоростью
    */
    {
        private int  x=0;
        private bool right = true;
        private int y = 120;
        private int x1 = 400;
        private int x2 = 450;
        private int x3 = 500;
        private int y1 = 0;
        private int y2 = 0;
        private int y3 = 0;
        private int timer_on_btn_stop=9;
        public MainForm()
        {
            InitializeComponent();
            this.Text = caption;
        }
        private int step =10;
        private String caption = "____________Работа с Таймерами!";
        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            btnStop.Enabled = false;
            timer_on_btn_stop -= 1;
                btnStop.Text = timer_on_btn_stop.ToString();
            if (timer_on_btn_stop <= 0)
            {
                btnStop.Text = "Stop";
                btnStop.Enabled = true;
            }

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Timer_time_now.Enabled = false;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            int MaxWidth = this.Width;//Включает рамку окна
            int MaxHeight = this.Height;    //Включает заголовок окна
            int WorkWidth = this.ClientRectangle.Width;
            int WorkHeigth = this.ClientRectangle.Height;
            if(lblMes.Left<WorkWidth-step-lblMes.Width) {
                lblMes.Left = lblMes.Left + step;
            }
            else {
                timer2.Stop();
                timer3.Start();
            }
            
        }

        private void btnTimer2_Click(object sender, EventArgs e)
        {
            timer2.Interval = 2;
            timer2.Start();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            int MinWidth = 0;
         
            if (lblMes.Left-step > MinWidth)
            {
                lblMes.Left = lblMes.Left - step;
            }
            else
            {
                timer3.Stop();
                timer2.Start();
            }
        }

        private void inheritedTimer4_Tick(object sender, EventArgs e)
        {
            if (this.Text.Length > 0)
            {
                String newCaption = this.Text;
                this.Text = newCaption.Remove(0, 1);//Начиная с символа 0 удалить 1 символ
                this.Update();//Обновление формы
            }
            else {
                this.Text = caption;
            }
            
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {

            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen mypen = new Pen(Color.Red, 3);//Pens.Red;
            Size size_rectangle_for_ellipse = new Size(50, 50);
            Point point_for_rectangle = new Point(x, y);
            Rectangle ellipse = new Rectangle(point_for_rectangle, size_rectangle_for_ellipse);
            gr.DrawEllipse(mypen, ellipse);
            Brush mybrush = Brushes.Aqua;
            gr.FillEllipse(mybrush, ellipse);

            Size size_rectangle_for_ellipse1 = new Size(10, 10);
            Size size_rectangle_for_ellipse2 = new Size(10, 10);
            Size size_rectangle_for_ellipse3 = new Size(10, 10);

            Point point_for_rectangle1 = new Point(x1, y1);
            Point point_for_rectangle2 = new Point(x2, y2);
            Point point_for_rectangle3 = new Point(x3, y3);

            Rectangle ellipse1 = new Rectangle(point_for_rectangle1, size_rectangle_for_ellipse1);
            Rectangle ellipse2 = new Rectangle(point_for_rectangle2, size_rectangle_for_ellipse2);
            Rectangle ellipse3 = new Rectangle(point_for_rectangle3, size_rectangle_for_ellipse3);

            gr.FillEllipse(mybrush, ellipse1);
            gr.FillEllipse(mybrush, ellipse2);
            gr.FillEllipse(mybrush, ellipse3);

        }

     

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Timer_for_ellipse.Enabled=true;
        }

        private void Timer_for_ellipse_Tick(object sender, EventArgs e)
        {
            y1 += 1;
            y2 += 2;
            y3 += 3;
            if (right == true)
                x += 1;
            else x = x - 1;
            int max = this.ClientRectangle.Width-50;//отмечаем размер ширины окна формы
            if (x == max)
                right = false;
            if (x == 0)
                right = true;
            this.Invalidate();//Обновление формы
        }

        private void btnStop_MouseClick(object sender, MouseEventArgs e)
        {

        }
    }
}
