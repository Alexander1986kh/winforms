﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsConsoleTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cBoxCity.SelectedItem = "Киев";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Привет, " + TxtName.Text);
            String messageText = String.Format("Привет, {0}!",
                TxtName.Text);
            string titleCaption = "Важное сообщение";
            DialogResult result=MessageBox.Show(messageText,//текст сообщения
                titleCaption,//Надпись-заголовок  окошка
                MessageBoxButtons.OK,//Типы кнопок в диалоге
                MessageBoxIcon.Information);//Тип окна диалога
            if (result != System.Windows.Forms.DialogResult.OK)
            {
                this.Text = "Это невозможно";//
            }
        }

        private void cBoxCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCity.Text = cBoxCity.Text;
        }

      

        private void btnComboBoxSelectedValue_Click(object sender, EventArgs e)
        {
        if(cBoxCity.SelectedItem=="Харьков")
        {
                MessageBox.Show("ВЫбранный город Харьков","Правильный выбор",
                 MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
        else {
                MessageBox.Show("ВЫбранный город -"+ cBoxCity.SelectedItem,
                "Выбор значения -",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
