﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsWindows
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();
            //Пример работы с модальными и немодальными окнами
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();//Модальное окно, все окна диалогов, пока не закроешь-дальше не сможешь работать
            form1.ShowDialog();//ShowDialog-вызов модального окна
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();//Обычное- перекрывающееся окно
            form1.Show();
        }

        private void MyForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.FillEllipse(Brushes.CadetBlue, 120, 150, 25, 25);
        }
    }
}
